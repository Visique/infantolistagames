var gulp = require('gulp');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var babel = require('gulp-babel');
var webpack = require('gulp-webpack');

gulp.task('default', () => {
    //var paths = ['src/**/*.js'];
    return gulp.src("src/index.js")
        .pipe(plumber())
        .pipe(babel())
        .pipe(webpack({
            module: {
                loaders: [
                    { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
                ]
            },
            devtool: 'source-map',
            debug: false,
            output: {
                filename: '[name].js',
            }
        }))
        .pipe(gulp.dest('dist'));

});

gulp.task('stream', function () {
    gulp.watch('src/**/*.js', ['default']);
});

gulp.task('copy', function() {
    gulp.src(['src/*.html', 'assets/**/*.json', 'src/**/images/*.*'])
        .pipe(gulp.dest('dist'));
});