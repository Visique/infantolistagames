var parseString = require('xml2js').parseString;
var glob = require('glob');
var fs = require('fs');
var path = require('path');

glob("../assets/retropie/**/*.xml", null, function (er, files) {
    files.forEach(function(value){
        fs.readFile(value, 'utf8', function (err,data) {
            parseString(data, {explicitArray: false} ,function (err, jsonData) {
                var resultData = { games: [] };

                //Parse
                jsonData.gameList.game.forEach(function(gameData) {
                    resultData.games.push({ name: gameData.name, thumbnail: gameData.thumbnail, publisher: gameData.publisher, developer: gameData.developer, releasedate: gameData.releasedate });
                });

                fs.writeFile(path.dirname(value)+'/gamelist.json', JSON.stringify(resultData), function (err) {
                    if (err) return console.log(err);
                });
            });
        });
        console.log(value);
    });
});
