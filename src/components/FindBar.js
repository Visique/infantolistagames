import React from "react";
import { Checkbox, FormControl } from 'react-bootstrap';

export default class FindBar extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = (event) => {
        this.props.onUserInput(
            event
        );
    }

    PlatformChange = (event) => {
        this.props.onPlatformChange(
            event.value, event.checked
        );
    }

    render() {
        return(
        <div style={{"padding": 5 + "px"}}>
            {
                this.props.Platforms.map((platform) => <Checkbox checked={this.props.platform} inline="false" name="platforms" value={platform} onChange={this.PlatformChange} children={platform}>{platform}</Checkbox>)
            }
            <span style={{"padding": "0px 10px"}}>
                Pesquisa: <input type="text" value={this.props.filterText} onChange={this.handleChange} name="filterText" />
            </span>
        </div>);
    }
}