import React from "react";
import GamesTable from './GamesTable';
import FindBar from './FindBar';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

export default class FilterableGameTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filterText: '',
            platforms: this.props.Platforms.slice()
        };
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    onPlatformChange = (value, checked) => {
        if(checked){
            this.setState((prevState, props) => {
                let myState = prevState.platforms;
                myState.push(value);
                return {platforms: myState};
            });
        }
        else
        {
            this.setState((prevState, props) => {
                let index = prevState.platforms.indexOf(value);
                let myState = prevState.platforms;
                if (index > -1) {
                    myState.splice(index, 1);
                }
                return {platforms: myState};
            });
        }
    }

    render() {
        const navbarInstance = (
            <Navbar fixedTop="true">
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">Infanto</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <FindBar Platforms={this.props.Platforms} PlatformsSelection={this.state.platforms} onUserInput={this.handleInputChange} onPlatformChange={this.onPlatformChange} filterText={this.state.filterText} />
            </Navbar>
        );

        return (
        <div>
            {navbarInstance}
            <div style={{"paddingTop": 50 + "px"}}>&nbsp;</div>
            <GamesTable GameData={this.props.GameData} Platforms={this.state.platforms} filterText={this.state.filterText} />
        </div>);
    }
}