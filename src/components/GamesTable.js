import React from "react";
import { Table } from 'react-bootstrap';
import dateFormat from 'dateformat';

export default class GamesTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let rows = [];
        let games = this.props.GameData;
        let platforms = this.props.Platforms;
        games.forEach((item) => {
            if (platforms.indexOf(item.platform) === -1) {
                return;
            }

            item.data.forEach((itemData) => {
                if (itemData.name.toLowerCase().indexOf(this.props.filterText.toLowerCase()) === -1) {
                    return;
                }

                let formatedDate = typeof itemData.releasedate == 'string' ? dateFormat(Date.parse(itemData.releasedate.substring(0,4)+'-'+itemData.releasedate.substring(4,6)+'-'+itemData.releasedate.substring(6,8)), "dd/mm/yyyy") : '';
                rows.push(
                <tr>
                    <td><img src={'./retropie/snes/'+itemData.thumbnail} /></td>
                    <td>{itemData.name}</td>
                    <td>{itemData.publisher}</td>
                    <td>{formatedDate}</td>
                </tr>
                );
            });
        });
        return(
        <Table striped bordered condensed hover>
            <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Publicadora</th>
                <th>Data lançamento</th>
            </tr>
            </thead>
            <tbody>
            {rows}
            </tbody>
        </Table>);
    }
}