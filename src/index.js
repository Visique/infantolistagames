import React from 'react';
import ReactDOM from 'react-dom';
import FilterableGameTable from './components/FilterableGameTable';
import request from 'SuperAgent';

let platforms = ['snes'];
let data = [];
let countLoaded = 0;

for (var i = 0; i < platforms.length; i++) {
    (() => {
        let internalValue = i;
        request
            .get('./retropie/'+platforms[internalValue]+'/gamelist.json')
            .set('Accept', 'application/json')
            .end(function(err, res){
                if (err || !res.ok) {
                    alert('Oh no! error');
                } else {
                    data.push({ platform: platforms[internalValue], data: res.body.games});

                    countLoaded++;

                    if(countLoaded == platforms.length)
                    {
                        PageLoaded();
                    }
                }
            });
    })();
}

function PageLoaded(){
    ReactDOM.render(
        <FilterableGameTable GameData={data} Platforms={platforms} />,
        document.getElementById('container')
    );
}